= L'estació espacial - estructura i passatgers
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

<<<

== Plantejament

Com a tècnic informàtic de la recent construïda estació espacial Nexus, l'Alt
Comandament t'ha encarregat la tasca de dissenyar una base de dades que
emmagatzemi informació sobre l'estructura de l'estació i els passatgers que
la visiten.

Els requeriments que t'han fet arribar són els següents:

- La nostra estació espacial està dividida en diferents seccions, que reben el
nom d'un color, per exemple, secció verda, secció blava, etc.

- Cada secció està subdividida en multitud de segments numerats, cosa que ens
permet referir-nos a un segment determinat com, per exemple, BLAU-123.

- Totes les sales de l'estació ocupen com a mínim un segment, però les sales
grans en poden ocupar més d'un. Per exemple, un restaurant podria ocupar
els segments BLAU-123 a BLAU-128. Els segments ocupats per una sala són sempre
consecutius i d'una única secció.

- Cada sala té un codi únic que la identifica, i es destina a un propòsit
específic, per exemple, habitacle, magatzem o restaurant. Cada sala té sortides
que la uneixen amb altres sales.

- La nostra estació allotja individus de diverses espècies alienígenes.
Necessitem saber a quina sala està allotjat cada passatger. A més, per cada sala
hem de saber per a quines espècies és apta l'atmosfera que hi ha creada al seu
interior.

- Dels passatgers en guardem el seu nom i espècie, un codi d'identificació únic
que els assignem el primer cop que vénen a l'estació, i la seva data d'arribada
i sortida de l'estació. Cal tenir en compte que un mateix passatger pot fer
diverses visites al llarg del temps, i que en volem tenir un registre.

- Disposem de diferents credencials de seguretat que permeten obrir les portes
que separen les sales. Un passatger amb una certa credencial de seguretat pot
obrir qualsevol porta que estigui marcada amb aquesta credencial. Una porta
pot estar marcada amb més d'una credencial, o bé amb cap, cosa que significa
que tothom la pot obrir. Un passatger pot tenir diverses credencials.

Amb aquestes dades dissenya un diagrama ER adequat. Compte! No volem que cap
passatger s'ofegui per un error en l'estructura de la base de dades!

== Diagrama solució

image::images/estacio_espacial_estructura.png[Solució]
