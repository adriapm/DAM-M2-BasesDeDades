= L'estació espacial - la duana
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

<<<

== Plantejament

L'Alt Comandament no està gens content amb alguns esdeveniments que han estat
tenint lloc a l'estació espacial Nexus recentment.

Sembla ser que alguns dels visitants estan utilitzant l'estació per al tràfic
de material il·legal. No cal dir el perill que suposa, per exemple, que algú
estigui traficant amb explosius en una llauna d'acer que flota al mig del buit
a més d'un milió de quilòmetres del planeta més proper.

Per ajudar a pal·liar aquest problema t'han encarregat una base de dades que
permeti fer un millor seguiment dels visitants.

Els requeriments són els següents:

- Fins ara podíem saber les dates d'entrada i sortida de cadascun dels
visitants, però no recordàvem on s'havia allotjat cada visitant en les seves
visites anteriors. Cal modificar l'esquema anterior per incorporar-hi aquesta
informació.

- Mantindrem un registre de totes les naus que atraquen a l'estació. De cada
nau en volem conèixer el seu nom, qui és el seu capità, quan ha arribat i quan
ha marxat de l'estació, quins passatgers porta a l'arribar, i quins hi porta
al marxar. També ens interessa saber d'on ve la nau i a on va quan surt d'aquí.

- Volem mantenir un registre d'això últim, per tal de poder consultar l'històric
d'una nau.

- A l'entrada de l'estació s'ha instal·lat una duana. Tots els visitants que
arriben o marxen de l'estació hi passen, i es registre el seu equipatge. Per
cada visitant que arribi a l'estació volem enregistrar el moment en què s'ha
revisat el seu equipatge i si s'ha produït cap incidència.

- Les incidències poden ser de dos tipus: alguna irregularitat en la
documentació o algun problema amb l'equipatge que porta. També permetrem
que hi hagi incidències en què no s'especifiqui cap d'aquests dos tipus, per si
apareixen contingències no previstes. Deixarem que el personal d'entrada pugui
redactar els detalls de la incidència.

- Començarem a enregistrar també el personal que treballa a l'estació. A part
de les dades que guardem de qualsevol visitant també voldrem guardar-ne la
data de contractació i, s'hi es dóna el cas, la data i el motiu d'acomiadament.

- En cada incidència que es registri al sistema hi ha de constar el membre o
membres del personal que l'han introduïda.

[TIP]
====
No cal partir de l'esquema anterior i ampliar-lo amb tot això. Podem crear un
esquema nou, i només copiar de l'esquema anterior les entitats que ens facin
falta en aquest.
====
