# UF1 - introducció a les bases de dades

## Model entitat-relació

### Índex

- [Teoria](model_er.adoc)

Activitats:

- [Activitats extraescolars](activitats_extraescolars.adoc)
- [Aules d'informàtica](aules_informatica.adoc)
- [Cinèfil](cinefil.adoc)
- [Concessionari](concessionari.adoc)
- [Fabricant](fabricant.adoc)
- [Eleccions](eleccions.adoc)
- [Protectora animals](protectora_animals.adoc)
- [Estació espacial - estructura i passatgers](estacio_espacial_estructura.adoc)
- [Xarxa social](xarxa_social.adoc)
- [Estació espacial - duana](estacio_espacial_duana.adoc)
- [Festival de música](festival.adoc)
- [Campeonat de gimnàstica rítmica](ritmica.adoc)
