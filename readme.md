Materials per al mòdul *M2 - Bases de dades* del cicle de *Desenvolupament d'Aplicacions Multiplataforma*
=====

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Materials per al mòdul M2 - Bases de dades</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/joanq/DAM-M2-BasesDeDades" property="cc:attributionName" rel="cc:attributionURL">https://gitlab.com/joanq/DAM-M2-BasesDeDades</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Índex
-----

### UF 1: introducció a les bases de dades

Aquest apartat està basat en el llibre __Database design - 2nd Editon__ de
*Adrienne Watt* i *Nelson Eng*, disponible sota una llicència CC-BY a la web
de [BCCampus](https://open.bccampus.ca/).

1. [Introducció a les bases de dades](UF1/1-Introduccio_a_les_BD)
2. [Model entitat-relacio](UF1/2-model_ER)
3. [Model relacional](UF1/3-model_relacional)
4. [Normalització](UF1/4-normalitzacio)

### UF 2: llenguatges SQL: DML i DDL

1. [Llenguatges de base de dades per manipular dades](UF2/1-DML)
2. [Llenguatges de la base de dades per crear l’estructura de la base de dades](UF2/2-DDL)
3. [Estratègies per al control de les transaccions i la concurrència](UF2/3-transaccions)

### UF 3: llenguatges SQL: DCL i extensió procedimental

1. [Gestió d'usuaris](UF3/1-usuaris)
2. Programació en bases de dades

### UF 4: bases de dades objecte-relacionals

1. Ús de bases de dades objecte-relacionals
