# Activitat: Consultes d'agregació

## Solucions

1. Consulta quants àlbums hi ha a la base de dades.

    ```sql
SELECT COUNT(*)
 FROM Album;
    ```

2. Cerca quants grups (_Artist_) hi ha el nom dels quals comenci per l'article
_The_.

    ```sql
SELECT COUNT(*)
 FROM Artist
 WHERE Name LIKE 'The %';
    ```

3. Amb dues consultes, cerca quants àlbums hi ha de Deep Purple.

    ```sql
SELECT ArtistId
 FROM Artist
 WHERE Name LIKE 'Deep Purple';
    ```

    ```sql
SELECT COUNT(*)
 FROM Album
 WHERE ArtistId=58;
    ```

4. Amb dues consultes, cerca els tres grups dels quals hi ha més àlbums.

    ```sql
SELECT COUNT(*) AS NAlbums, ArtistId
 FROM Album
 GROUP BY ArtistId
 ORDER BY NAlbums DESC
 LIMIT 3;
    ```

    ```sql
SELECT Name FROM Artist
 WHERE ArtistId IN (90,22,58);
    ```

5. Cerca la durada mitjana de totes les pistes. Volem el resultat en segons.

    ```sql
SELECT AVG(Milliseconds)/1000
 FROM Track;
    ```

6. Amb tres consultes, cerca els 5 àlbums que tenen una durada mitjana de les
seves pistes més gran. De cada àlbum en volem el seu nom i el nom del grup.

    ```sql
SELECT AVG(Milliseconds) AS DuradaMitjana, AlbumId
 FROM Track
 GROUP BY AlbumId
 ORDER BY DuradaMitjana DESC
 LIMIT 5;
    ```

    ```sql
SELECT Title, ArtistId
 FROM Album
 WHERE AlbumId IN (253,227,229,231,226);
    ```

    ```sql
SELECT ArtistId, Name
 FROM Artist
 WHERE ArtistId IN (147,149,158);
    ```

7. Amb dues consulta, cerca el nom dels tres gèneres dels quals en tenim més
pistes.

    ```sql
SELECT COUNT(*) AS N, GenreId
 FROM Track
 GROUP BY GenreId
 ORDER BY N DESC
 LIMIT 3;
    ```

    ```sql
SELECT Name
 FROM Genre
 WHERE GenreId IN (1,7,3);
    ```

8. Cerca quantes pistes hi ha en què hi consti algun compositor.

    ```sql
SELECT COUNT(*)
 FROM Track
 WHERE Composer IS NOT NULL;
    ```

9. Cerca quants minuts de música disposem del compositor Johann Sebastian Bach.

    ```sql
SELECT SUM(Milliseconds)/60000
 FROM Track
 WHERE Composer LIKE 'Johann Sebastian Bach';
    ```

10. Cerca el preu mitjà per pista. Després, cerca quantes pistes hi ha de
cadascun dels preus.

    ```sql
SELECT AVG(UnitPrice)
 FROM Track;
    ```

    ```sql
SELECT UnitPrice, COUNT(*)
 FROM Track
 GROUP BY UnitPrice;
    ```

11. Selecciona els MB que ocupa cadascun dels àlbums. Ordena els resultats
de més a menys MB.

    ```sql
SELECT AlbumId, SUM(Bytes)/1024/1024 AS MB
 FROM Track
 GROUP BY AlbumId
 ORDER BY MB DESC;
    ```

12. Amb dues consultes, obté el nom de les llistes (_playlist_) que contenen
més de mil cançons.

    ```sql
SELECT PlaylistId, COUNT(*) AS NumSongs
 FROM PlaylistTrack
 GROUP BY PlaylistId
 HAVING NumSongs > 1000;
    ```

    ```sql
SELECT Name
 FROM Playlist
 WHERE PlaylistId IN (1,5,8);
    ```

13. Descobreix quants clients tenen alguna dada introduïda a _State_. Fes-ho de
dues maneres diferents.

    ```sql
SELECT COUNT(State)
 FROM Customer;
    ```

    ```sql
SELECT COUNT(*)
 FROM Customer
 WHERE State IS NOT NULL;
    ```

14. Descobreix quants clients tenim de cadascuna de les ciutats de Brasil.

    ```sql
SELECT City, COUNT(*)
 FROM Customer
 WHERE Country LIKE 'Brazil'
 GROUP BY City;
    ```

15. Descobreix de quines ciutats d'Estats Units i de França tenim més d'un
client. Volem saber-ne el nom, si són d'un país o de l'altre, i quants clients
hi ha.

    ```sql
SELECT Country, City, COUNT(*) AS NumCustomers
 FROM Customer
 WHERE Country LIKE 'France'
  OR Country LIKE 'USA'
 GROUP BY City
 HAVING NumCustomers>1;
    ```

16. Amb dues consultes, troba quins països tenen exactament la mateixa
quantitat de factures (_invoice_) que el país que en té menys.

    ```sql
SELECT COUNT(*) AS NumInvoices
 FROM Invoice
 GROUP BY BillingCountry
 ORDER BY NumInvoices
 LIMIT 1;
    ```

    ```sql
SELECT BillingCountry, COUNT(*) AS NumInvoices
 FROM Invoice
 GROUP BY BillingCountry
 HAVING NumInvoices=7;
    ```

17. Utilitzant només la taula _InvoiceLine_, calcula el preu total de la
factura número 4. Després, comprova que la informació coincideix amb la que
hi ha a la taula _Invoice_.

    ```sql
SELECT SUM(UnitPrice*Quantity)
 FROM InvoiceLine
 WHERE InvoiceId=4;
    ```

    ```sql
SELECT Total
 FROM Invoice
 WHERE InvoiceId=4;
    ```

18. Utilitzant només la taula _InvoiceLine_, cerca quines factures tenen un
preu total superior o igual a 20.

    ```sql
SELECT InvoiceId, SUM(UnitPrice*Quantity) AS Total
 FROM InvoiceLine
 GROUP BY InvoiceId
 HAVING Total >= 20;
    ```

19. Cerca els identificadors de les cançons que ens han reportat més de 2 dólars
de beneficis.

    ```sql
SELECT TrackId, SUM(UnitPrice*Quantity) AS Total
 FROM InvoiceLine
 GROUP BY TrackId
 HAVING Total>=2;
    ```

20. Cerca des de quins països es fan comandes que, de mitjana, superen els 6
dólars de preu.

    ```sql
SELECT BillingCountry, AVG(Total) AS Average
 FROM Invoice
 GROUP BY BillingCountry
 HAVING Average>6;
    ```

21. Sabent que la funció YEAR ens permet seleccionar l'any d'una data, troba
quants diners hem cobrat cada any. Ordena els resultats de més guanys a menys.

    ```sql
SELECT YEAR(InvoiceDate) AS Year, SUM(Total) AS Sum
 FROM Invoice
 GROUP BY Year
 ORDER BY Sum DESC;
    ```

22. Sabent que la funció MONTH ens permet seleccionar el mes d'una data,
utilitza dues consultes per trobar per quins mesos la mitjana del preu de les
factures supera la mitjana global. Ordena els resultats de mitjanes més gran a
mitjanes més petites.

    ```sql
SELECT AVG(Total)
 FROM Invoice;
    ```

    ```sql
SELECT MONTH(InvoiceDate) AS Month, AVG(Total) AS Average
 FROM Invoice
 GROUP BY Month
 HAVING Average>5.651942
 ORDER BY Average DESC;
    ```
