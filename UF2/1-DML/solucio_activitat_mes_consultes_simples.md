# Activitat: Més consultes simples

## Solucions

### Consultes bàsiques

1. Obté la data de la comanda, la data d'enviament i l'adreça d'enviament de
totes les comandes enviades a la ciutat de New York.

    ```sql
SELECT order_date, shipped_date, ship_address
 FROM orders
 WHERE ship_city LIKE 'New York';
    ```

2. Obté l'identificador, el nom de la persona receptora de l'enviament, i
l'identificador de client, de totes les comandes per les quals no consta una
data d'enviament.

    ```sql
SELECT id, ship_name, customer_id
 FROM orders
 WHERE shipped_date IS NULL;
    ```

3. Obté l'identificador, la data de la comanda, i l'identificador de client,
de totes les comandes que es van fer abans del maig de 2006.

    ```sql
SELECT id, order_date, customer_id
 FROM orders
 WHERE order_date<'2006-05-01';
    ```

4. Obté l'identificador, la data de la comanda, i la ciutat de destí de totes
les comandes les despeses d'enviament de les quals siguin superiors a 100
dòlars.

    ```sql
SELECT id, order_date, ship_city
 FROM orders
 WHERE shipping_fee>100;
    ```

5. Obté l'identificador, i l'adreça i ciutat d'enviament de totes les comandes
que siguin posteriors al 15 de maig de 2006 i que tinguin un enviament
gratuït.

    ```sql
SELECT id, ship_address, ship_city
 FROM orders
 WHERE order_date>'2006-05-15'
  AND shipping_fee=0;
    ```

6. Obté l'identificador, el nom, i el cognom de tots els clients el nom dels
quals comença per A i que viuen a Memphis.

    ```sql
SELECT id, first_name, last_name
 FROM customers
 WHERE first_name LIKE 'A%'
  AND city LIKE 'Memphis';
    ```

7. Obté l'identificador del producte, la quantitat de productes comprats i
el preu de cada unitat, de la comanda amb número 42 (utilitza la taula
*order_details*).

    ```sql
SELECT product_id, quantity, unit_price
 FROM order_details
 WHERE order_id=42;
    ```

8. Obté l'identificador de totes les comandes que no estiguin tancades. Les
comandes tancades tenen un 2 a la columna *status_id* de la taula
*order_details*. Utilitza `SELECT DISTINCT` per evitar resultats repetits.
Ordena els resultats de menor a major.

    ```sql
SELECT DISTINCT order_id
 FROM order_details
 WHERE status_id!=2;
    ```

9. Obté l'identificador, l'identificador de la comanda, i l'identificador del
producte de totes les comandes (a *order_details*) que no estiguin associades
a cap ítem d'inventari, o que estiguin associades a un ítem d'inventari de
número superior a 130.

    ```sql
SELECT id, order_id, product_id
 FROM order_details
 WHERE inventory_id IS NULL
  OR inventory_id>130;
    ```

10. Obté l'identificador, l'identificador de la comanda, l'identificador del
producte, la quantitat comprada, el preu per unitat, i el preu total dels
detalls de comanda el preu total dels quals sigui superior a 1000 però inferior
a 10000.

    ```sql
SELECT id, order_id, product_id, quantity, unit_price, quantity*unit_price
 FROM order_details
 WHERE quantity*unit_price > 1000
  AND quantity*unit_price < 10000;
    ```

11. Obté l'identificador, el codi de producte i el nom del producte, dels
productes el preu de venta dels quals sigui menys d'un 30% superior al preu de
cost.

    ```sql
SELECT id, product_code, product_name
 FROM products
 WHERE list_price < 1.3 * standard_cost;
    ```

12. Obté l'identificador, el codi de producte i el nom del producte, dels
productes que no siguin begudes (*Beverages*) i que tinguin un preu de venta de
menys de 10 dòlars.

    ```sql
SELECT id, product_code, product_name
 FROM products
 WHERE category NOT LIKE 'Beverages'
  AND list_price < 10;
    ```

13. Obté l'identificador, el codi de producte i el nom del producte, dels
productes que continguin la combinació 'ea' en el seu nom, però que no hi
tinguin més de tres erres.

    ```sql
SELECT id, product_code, product_name
 FROM products
 WHERE product_name LIKE '%ea%'
  AND product_name NOT LIKE '%r%r%r%r%';
    ```

14. Obté l'identificador, l'identificador del client, i la data de comanda, de
totes les comandes pagades amb targeta de crèdit i enviades als estats de
Nova York (NY) o Oregon (OR).

    ```sql
SELECT id, customer_id, order_date
 FROM orders
 WHERE payment_type LIKE 'Credit Card'
  AND (ship_state_province LIKE 'NY'
   OR ship_state_province LIKE 'OR');
    ```

15. Obté l'identificador, el nom, i el cognom, de tots els client que tenen el
nom o el cognom acabat en erra, però no els dos. Ordena els resultats pel
cognom.

    ```sql
SELECT id, first_name, last_name
 FROM customers
 WHERE (first_name LIKE '%r'
   OR last_name LIKE '%r')
  AND (first_name NOT LIKE '%r'
   OR last_name NOT LIKE '%r')
 ORDER BY last_name;
    ```

    ```sql
 SELECT id, first_name, last_name
  FROM customers
  WHERE (first_name LIKE '%r'
    AND last_name NOT LIKE '%r')
   OR (first_name NOT LIKE '%r'
    AND last_name LIKE '%r')
  ORDER BY last_name;
    ```

16. Obté l'identificador i el nom dels productes pels quals hi ha més d'un
proveïdor.

    ```sql
SELECT id, supplier_ids
 FROM products
 WHERE supplier_ids LIKE '%;%';
    ```

17. Obté l'identificador i el nom dels productes que són proporcionats pel
proveïdor 3. La consulta ha de mostrar també els productes pels quals hi ha
més d'un proveïdor i el proveïdor 3 és un d'ells. Utilitza el fet que sabem
que només hi ha 10 proveïdors diferents.

    ```sql
SELECT id, product_name
 FROM products
 WHERE supplier_ids LIKE '%3%';
    ```

18. Obté l'identificador i el nom dels productes pels quals la quantitat que
es demana quan es fa una comanda al proveïdor (*reorder_level*) no coincideix
amb la quantitat mínima que s'ha de demanar (*minimum_reorder_quantity*).

    ```sql
SELECT id, product_name
 FROM products
 WHERE reorder_level != minimum_reorder_quantity
  OR (reorder_level IS NULL AND minimum_reorder_quantity IS NOT NULL)
  OR (reorder_level IS NOT NULL AND minimum_reorder_quantity IS NULL);
    ```

19. Mostra el nom, cognom, adreça i ciutat de residència de tots els clients,
ordenats pel cognom, de l'A a la Z, després pel nom, també d'A a la Z, i
finalment per la ciutat de residència, de la Z a la A.

    ```sql
SELECT first_name, last_name, address, city
 FROM customers
 ORDER BY last_name, first_name, city DESC;
    ```

20. Selecciona totes les comandes que encara no s'han completat
(*status_id* val 3 si s'ha completat), i que es van fer entre el 15 de maig de
2006 i el 15 de juny de 2006. D'aquestes comandes en volem saber
l'identificador, l'identificador del client i de l'empleat, la data de la
comanda, i el nom a qui s'han enviat. Ordena els resultats per l'identificador
del client primer, i en segon lloc per l'identificador de l'empleat.

    ```sql
SELECT id, customer_id, employee_id, order_date, ship_name
 FROM orders
 WHERE status_id != 3
  AND order_date BETWEEN '2006-05-15' AND '2006-06-15'
 ORDER BY customer_id, employee_id;
    ```
