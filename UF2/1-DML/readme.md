# UF2 - llenguatges SQL: DML i DDL

## Data Manipulation Language

### Índex

- [Teoria](dml.adoc)

Activitats:

- [Primeres passes en MariaDB](activitat_primeres_passes_mariadb.md)
- [Tipus de dades](activitat_tipus_de_dades.md)
- [Consultes simples](activitat_consultes_simples.md)
- [Més consultes simples](activitat_mes_consultes_simples.md)
- [Consultes complexes](activitat_consultes_complexes.md)
- [Funcions integrades](activitat_funcions_integrades.md)
- [Subconsultes](activitat_subconsultes.md)
- [Modificació de dades](activitat_modificacio.md)
