# Activitat: Consultes de funcions integrades

Base de dades Sakila.

## Solucions

1. Volem saber quants diners hem fet en total per a cada pel·lícula (per cada
títol diferent, no per cada exemplar que tenim) i per cada botiga. El resultat
ha de ser una llista de títols de pel·lícula, números de botigues, i
quantitats.

    **Pista:** es poden fer GROUP BY per més d'una columna.

    ```sql
SELECT title, i.store_id, SUM(amount) AS income
  FROM payment p
  JOIN rental r ON r.rental_id=p.rental_id
  JOIN inventory i ON i.inventory_id=r.inventory_id
  JOIN film f ON f.film_id=i.film_id
  GROUP BY f.film_id,i.store_id
  ORDER BY title;
    ```

2. Cerca el nom i cognom de tots els clients que tenen alguna pel·lícula
llogada. També volem saber el nom de la pel·lícula que tenen en lloguer, el
número d'ítem d'inventari de què es tracta, i la data en què es va llogar.
Ordena els resultats per la data de lloguer.

    **Pista:** La taula *rental* conté un NULL a la columna *return_date* si un
    lloguer no s'ha retornat encara.

    ```sql
SELECT first_name, last_name, title, i.inventory_id, r.rental_date
  FROM customer c
  JOIN rental r ON r.customer_id=c.customer_id
  JOIN inventory i ON i.inventory_id=r.inventory_id
  JOIN film f ON f.film_id=i.film_id
  WHERE r.return_date IS NULL
  ORDER BY r.rental_date;
    ```

3. Una client de nom Kelly Torres ens pregunta si mai ha llogat la
pel·lícula de títol "ALASKA PHANTOM". Fes una consulta que ens permeti
contestar a aquesta pregunta. Si la resposta és sí, volem que la consulta ens
mostri la data en què la va llogar.

    ```sql
SELECT r.rental_date
  FROM rental r
  JOIN customer c ON r.customer_id=c.customer_id
  JOIN inventory i ON i.inventory_id=r.inventory_id
  JOIN film f ON f.film_id=i.film_id
  WHERE c.first_name LIKE 'Kelly'
    AND c.last_name LIKE 'Torres'
    AND f.title LIKE 'ALASKA PHANTOM';
    ```

4. Volem una llista que ens digui quantes pel·lícules s'han llogat cada mes,
des del juliol de 2005 fins a l'abril de 2006.

    ```sql
SELECT COUNT(*) AS n_rentals, MONTH(rental_date) as month, YEAR(rental_date) AS year
  FROM rental
  WHERE rental_date >= '2005-07-01 00:00:00'
   AND rental_date < '2006-05-01 00:00:00'
  GROUP BY month, year;
    ```

5. Volem una llista de tots els lloguers que es van fer entre el 15 i el 20 de
juny de 2005. Per cada lloguer volem veure el nom i cognom del client, el
títol de la pel·lícula llogada, la data del lloguer, la data de retorn (si s'ha
retornat), i la data màxima de retorn. Ordena els resultats per la data de
lloguer.

    **Pista:** la data màxima de retorn es calcula com la data en què es fa el
    lloguer més el nombre de dies que s'indica a la columna *rental_duration*
    de la taula *film*.

    ```sql
SELECT first_name, last_name, title, rental_date, return_date, ADDDATE(rental_date, rental_duration)
  FROM customer c
  JOIN rental r ON c.customer_id=r.customer_id
  JOIN inventory i ON i.inventory_id=r.inventory_id
  JOIN film f ON f.film_id=i.film_id
  WHERE rental_date BETWEEN '2005-06-15' AND '2005-06-21'
  ORDER BY rental_date;
    ```

6. Volem saber quines són les hores en què més lloguers es fan. Mostra un
recompte de quants lloguers s'han donat en total per cada hora del dia. Ordena
el resultat per l'hora.

    ```sql
SELECT HOUR(rental_date) AS hour, COUNT(*) AS n_rentals
  FROM rental
  GROUP BY hour
  ORDER BY hour;
    ```

7. Volem una llista de quants lloguers s'han fet cada dia d'abril i maig
de 2005. Ordena els resultats per la data.

    ```sql
SELECT DATE(rental_date) as day, COUNT(*) AS n_rentals
  FROM rental
  WHERE rental_date BETWEEN '2005-04-01' AND '2005-06-01'
  GROUP BY day
  ORDER BY day;
    ```

8. Volem saber qui és l'actor preferit d'en Austin Cintron. Per això mirarem
els actors que actuen a les pel·lícules que ha llogat i comptarem quants cops
apareix cada actor. Considerarem que el seu actor preferit és el que apareix més
vegades.

    ```sql
SELECT COUNT(*) AS n_films, a.first_name, a.last_name
  FROM customer c
  JOIN rental r ON c.customer_id=r.customer_id
  JOIN inventory i ON i.inventory_id=r.inventory_id
  JOIN film f ON f.film_id=i.film_id
  JOIN film_actor fa ON fa.film_id=f.film_id
  JOIN actor a ON a.actor_id=fa.actor_id
  WHERE c.first_name LIKE 'Austin'
    AND c.last_name LIKE 'Cintron'
  GROUP BY a.actor_id
  ORDER BY n_films DESC
  LIMIT 1;
    ```

9. Volem obtenir una llista que ens mostri les ciutats per les quals tenim
més d'un client. Volem veure quants clients hi ha a cadascuna d'aquesta ciutats,
amb els seus països respectius, i volem la llista ordenada pel nombre de
clients.

    ```sql
SELECT COUNT(*) AS n_customers, city, country
  FROM customer c
  JOIN address a ON c.address_id=a.address_id
  JOIN city ON city.city_id=a.city_id
  JOIN country ON country.country_id=city.country_id
  GROUP BY city.city_id
  HAVING n_customers>1
  ORDER BY n_customers DESC;
    ```

10. Volem obtenir una llista que mostri totes les categories de totes les
pel·lícules el títol de les quals conté la paraula 'AFFAIR'.

    ```sql
SELECT f.title, c.name
  FROM film f
  JOIN film_category fc ON f.film_id=fc.film_id
  JOIN category c ON c.category_id=fc.category_id
  WHERE f.title LIKE '%AFFAIR%';
    ```

11. Volem obtenir una llista amb tots els lloguers que s'han retornat fora de
termini. Per a cada lloguer volem veure el seu identificador, la data de
lloguer, la data de retorn, la quantitat que s'ha pagat per aquest lloguer, i
amb quants dies de retard s'ha tornat el lloguer.

    ```sql
SELECT r.rental_id, r.rental_date, r.return_date, f.rental_duration, SUM(p.amount), DATEDIFF(r.return_date, ADDDATE(r.rental_date, f.rental_duration)) AS late_days
  FROM rental r
  JOIN inventory i ON i.inventory_id=r.rental_id
  JOIN film f ON f.film_id=i.film_id
  JOIN payment p ON p.rental_id=r.rental_id
  GROUP BY r.rental_id
  HAVING late_days>0;
    ```

12. Cerca els exemplars que tenim disponibles de la pel·lícula
"SUSPECTS QUILLS", és a dir, els exemplars que no estan llogats en aquest
moment. Per a cada exemplar volem saber-ne el seu identificador, l'identificador
de la botiga on es troba, i l'adreça completa on és aquesta botiga (adreça,
ciutat i país).

    ```sql
SELECT i.inventory_id, i.store_id, a.address, a.address2, c.city, co.country
  FROM inventory i
  LEFT JOIN rental r ON r.inventory_id=i.inventory_id
  LEFT JOIN film f ON f.film_id=i.film_id
  LEFT JOIN store s ON s.store_id=i.store_id
  LEFT JOIN address a ON a.address_id=s.address_id
  LEFT JOIN city c ON c.city_id=a.city_id
  LEFT JOIN country co ON co.country_id=c.country_id
  WHERE f.title LIKE 'SUSPECTS QUILLS'
  GROUP BY i.inventory_id
  HAVING MAX(r.rental_date)<MAX(r.return_date) OR COUNT(r.rental_id)=0
  ORDER BY i.inventory_id;
    ```

13. Volem obtenir l'historial de lloguers de tots els exemplars de la
pel·lícula "JEKYLL FROGMEN". La llista ha d'incloure l'ítem d'inventari, el
nom i cognom del client que l'ha llogada, la data en què l'ha llogada i la data
de retorn (si s'ha retornat). Ordena els resultats per l'identificador de
l'inventari, i després per la data de lloguer.

    ```sql
SELECT i.inventory_id, c.first_name, c.last_name, r.rental_date, r.return_date
  FROM inventory i
  JOIN rental r ON r.inventory_id=i.inventory_id
  JOIN film f ON f.film_id=i.film_id
  JOIN customer c ON c.customer_id=r.customer_id
  WHERE f.title LIKE 'JEKYLL FROGMEN'
  ORDER BY i.inventory_id, r.rental_date;
    ```

14. Demostra que la contrasenya de l'empleat Mike és '12345' i que s'ha xifrat
utilitzant l'agorisme SHA.

    ```sql
SELECT staff_id, first_name, last_name, username
  FROM staff
  WHERE BINARY password=SHA('12345');
    ```

15. Cerca si hi ha algun exemplar que no s'hagi llogat mai. Volem saber a quina
botiga es troba (només l'identificador), i de quina pel·lícula es tracta.

    ```sql
SELECT i.inventory_id, i.store_id, f.title
  FROM inventory i
  LEFT JOIN rental r ON i.inventory_id=r.rental_id
  JOIN film f ON f.film_id=i.film_id
  GROUP BY r.rental_id
  HAVING COUNT(r.rental_id)=0;
    ```

16. Fes una consulta que ens mostri l'idioma original de les pel·lícules.
Si el camp *original_language_id* és NULL vol dir que l'idioma original és el
mateix en què està la pel·lícula, i que es guarda a *language_id*. En canvi,
si *original_language_id* és diferent de NULL, *language_id* guarda l'idioma
a què s'ha doblat la pel·lícula.

    Per a cada pel·lícula volem veure el seu títol i el nom de l'idioma
    original.

    ```sql
SELECT f.title, IFNULL(l2.name, l.name)
  FROM film f
  LEFT JOIN language l ON l.language_id=f.language_id
  LEFT JOIN language l2 ON l2.language_id=f.original_language_id;
    ```
