# Activitat: Consultes simples

## Base de dades

Per aquests exercicis utilitzarem la base de dades NorthWind. Aquesta base de
dades s'utilitza com a base de dades de mostra per *MS Access* i ha
estat portada a MySQL. Es pot descarregar a https://github.com/dalers/mywind.

Cal baixar els fitxers *northwind.sql* i *northwind-data.sql*, i importar-los
al MySQL/MariaDB.

Aquesta base de dades és un bon exemple de base de dades per a una petita
empresa. Inclou els empleats i els clients, les comandes, l'inventari, gestió
de compres, proveïdors, i transport.

Per aquests exercicis només utilitzarem les taules *employees* i *products*.

## Consultes bàsiques

1. Seleccionar de la taula *employees* tots els empleats de Redmond. S'han
d'ordenar els resultats pel  cognom. Les dades que s'han de mostrar són
l'identificador d'empleat, el cognom, i la posició laboral que ocupa.

2. Mostra els empleats el cognom dels quals comenci per "C".

3. Mostra els cognoms de la taula *employees* que tinguin una "e" en la segona
posició.

4. Mostra els noms de la taula *employees* que comencin per "M" i que continguin
una "a".

5. Mostra el nom dels productes de la taula *products* que siguin del proveidor
número 4 o 10. Escriu la sentència de dues formes diferents.

6. Mostra el nom dels productes de la taula *products* que **NO** siguin ni del
proveidor 4 ni del 10. Escriu la sentència de dues formes diferents.

7. Mostra el cognom dels empleats la posició dels quals és "Sales Manager",
"Sales Representative", o "Sales Coordinator". Escriu la sentència de dues
formes fent servir dos operadors diferents.

8. Mostra els cognoms dels empleats que no ocupin cap de les tres posicions
anteriors. Escriu la sentència de dues formes fent servir dos operadors
diferents.

9. Obté el nom i el preu dels productes el preu dels quals està comprès entre
10 i 20. Escriu la sentència de dues formes diferents.

10. Obté el nom i el preu dels productes el preu dels quals **NO** està
comprès entre 10 i 20. Escriu la sentència de dues formes diferents.

11. A partir de la taula *products* obté el nom dels productes, el preu i el
número del proveïdor que tinguin un preu superior a 20 i que siguin del
proveïdor número 4 o número 10. Escriu la sentència de dues formes diferents.

12. Mostra el nom, cost i preu dels productes pels quals la diferència entre el
preu de cost i el preu de venta sigui de més de 10.

## Consultes extres

1. Selecciona la llista de productes que contenen la paraula 'soup' en el seu
nom. Escriu la consulta de manera que funcioni correctament independentment de
si la paraula 'soup' s'escriu en majúscules o en minúscules.

2. Mostra una llista dels empleats de Redmond que sàpiguen francès (es diu a
  les notes), i de Seatle que no en sàpiguen.

3. Fes una llista de les comandes que es van servir al client número 4.
Després, treu una llista d'aquestes comandes que, a més, han tingut un cost
d'enviament de més de 4 dòlars.

## Solucions

[Solucions](solucio_activitat_consultes_simples.md)
