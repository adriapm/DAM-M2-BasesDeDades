# Activitat: Consultes simples

## Solucions

### Consultes bàsiques

1. Seleccionar de la taula *employees* tots els empleats de Redmond. S'han
d'ordenar els resultats pel  cognom. Les dades que s'han de mostrar són
l'identificador d'empleat, el cognom, i la posició laboral que ocupa.

    ```sql
SELECT id, last_name, job_title
 FROM employees
 WHERE city LIKE 'Redmond'
 ORDER BY last_name;
    ```

2. Mostra els empleats el cognom dels quals comenci per "C".

    ```sql
SELECT id, first_name, last_name
 FROM employees
 WHERE last_name LIKE 'C%';
    ```

3. Mostra els cognoms de la taula *employees* que tinguin una "e" en la segona
posició.

    ```sql
SELECT last_name
 FROM employees
 WHERE last_name LIKE '_e%';
    ```

4. Mostra els noms de la taula *employees* que comencin per "M" i que continguin
una "a".

    ```sql
SELECT first_name
 FROM employees
 WHERE first_name LIKE 'M%a%';
    ```

5. Mostra el nom dels productes de la taula *products* que siguin del proveidor
número 4 o 10. Escriu la sentència de dues formes diferents.

    ```sql
SELECT product_name, supplier_ids
 FROM products
 WHERE supplier_ids = '4'
  OR supplier_ids = '10';
    ```

    ```sql
SELECT product_name, supplier_ids
 FROM products
 WHERE supplier_ids IN ('4','10');
    ```

6. Mostra el nom dels productes de la taula *products* que **NO** siguin ni del
proveidor 4 ni del 10. Escriu la sentència de dues formes diferents.

    ```sql
SELECT product_name, supplier_ids
 FROM products
 WHERE supplier_ids != '4'
  AND supplier_ids != '10';
    ```

    ```sql
SELECT product_name, supplier_ids
 FROM products
 WHERE supplier_ids NOT IN ('4','10');
    ```

7. Mostra el cognom dels empleats la posició dels quals és "Sales Manager",
"Sales Representative", o "Sales Coordinator". Escriu la sentència de dues
formes fent servir dos operadors diferents.

    ```sql
SELECT last_name
 FROM employees
 WHERE job_title LIKE 'Sales Manager'
  OR job_title LIKE 'Sales Representative'
  OR job_title LIKE 'Sales Coordinator';
    ```

    ```sql
SELECT last_name
 FROM employees
 WHERE job_title IN ('Sales Manager',
      'Sales Representative','Sales Coordinator');
    ```

8. Mostra els cognoms dels empleats que no ocupin cap de les tres posicions
anteriors. Escriu la sentència de dues formes fent servir dos operadors
diferents.

    ```sql
SELECT last_name
 FROM employees
 WHERE job_title NOT LIKE 'Sales Manager'
  AND job_title NOT LIKE 'Sales Representative'
  AND job_title NOT LIKE 'Sales Coordinator';
    ```

    ```sql
SELECT last_name
 FROM employees
 WHERE job_title NOT IN ('Sales Manager',
      'Sales Representative','Sales Coordinator');
    ```

9. Obté el nom i el preu dels productes el preu dels quals està comprès entre
10 i 20. Escriu la sentència de dues formes diferents.

    ```sql
SELECT product_name, list_price
 FROM products
 WHERE list_price>=10
  AND list_price<=20;
    ```

    ```sql
SELECT product_name, list_price
 FROM products
 WHERE list_price BETWEEN 10 AND 20;
    ```

10. Obté el nom i el preu dels productes el preu dels quals **NO** està
comprès entre 10 i 20. Escriu la sentència de dues formes diferents.

    ```sql
SELECT product_name, list_price
 FROM products
 WHERE list_price<10
  OR list_price>20;
    ```

    ```sql
SELECT product_name, list_price
 FROM products
 WHERE list_price NOT BETWEEN 10 AND 20;
    ```

11. A partir de la taula *products* obté el nom dels productes, el preu i el
número del proveïdor que tinguin un preu superior a 20 i que siguin del
proveïdor número 4 o número 10. Escriu la sentència de dues formes diferents.

    ```sql
SELECT product_name, list_price, supplier_ids
 FROM products
 WHERE list_price > 20
  AND (supplier_ids='4' OR supplier_ids='10');
    ```

    ```sql
SELECT product_name, list_price, supplier_ids
 FROM products
 WHERE list_price > 20
  AND supplier_ids IN ('4','10');
    ```

12. Mostra el nom, cost i preu dels productes pels quals la diferència entre el
preu de cost i el preu de venta sigui de més de 10.

    ```sql
SELECT product_name, standard_cost, list_price
 FROM products
 WHERE list_price - standard_cost > 10;
    ```

### Consultes extres

1. Selecciona la llista de productes que contenen la paraula 'soup' en el seu
nom. Escriu la consulta de manera que funcioni correctament independentment de
si la paraula 'soup' s'escriu en majúscules o en minúscules.

    ```sql
SELECT id, product_name
 FROM products
 WHERE product_name LIKE '%soup%';
    ```

2. Mostra una llista dels empleats de Redmond que sàpiguen francès (es diu a
  les notes), i de Seatle que no en sàpiguen.

    ```sql
SELECT id, first_name, last_name, city, notes
 FROM employees
 WHERE (
   city LIKE 'Redmond'
   AND notes LIKE '%French%'
 ) OR (
   city LIKE 'Seattle'
   AND (
    notes NOT LIKE '%French%'
    OR notes IS NULL
   )
 );
    ```

3. Fes una llista de les comandes que es van servir al client número 4.
Després, treu una llista d'aquestes comandes que, a més, han tingut un cost
d'enviament de més de 4 dòlars.

    ```sql
SELECT id, order_date
 FROM orders
 WHERE customer_id = 4;
    ```

    ```sql
SELECT id, order_date, shipping_fee
 FROM orders
 WHERE customer_id = 4
 AND shipping_fee > 4;
    ```
